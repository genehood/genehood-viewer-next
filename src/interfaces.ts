/* eslint camelcase: 1 */
interface IGn {
  order: number[]
  ref: string
  refStrand: '+' | '-'
  pos: {
    refStart: number
    start: number
    stop: number
  }
}

interface IHomologyGroup {
  color: string
  hash: string
  evalue: number
  size: number
}

interface IGh {
  refStart: number
  refStrand: '+' | '-'
  ref: string
}

interface IGene {
  gh: IGh
  aseq_id: string
  stable_id: string
  start: number
  stop: number
  strand: '+' | '-'
  representation: {
    selected: boolean
    groups: IHomologyGroup[]
    mask: boolean
  }
}

export { IGene, IGh, IGn, IHomologyGroup }
