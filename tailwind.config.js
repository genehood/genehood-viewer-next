/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  purge: false,
  theme: {
    extend: {
      fontFamily: {
        title: ['Impact'],
        roboto: ['Roboto', 'sans-serif'],
        cascadia: ['Cascadia', 'mono'],
        block: ['Blockschrift', 'sans-serif'],
        gligoth: ['Gligoth', 'mono']
      },
      width: {
        '72': '18rem',
        '86': '21rem',
        '100': '24rem'
      },
      fontSize: {
        '2xs': '0.65rem',
        '3xs': '0.5rem'
      },
      colors: {
        splash: {
          bg: '#17202C'
        }
      }
    }
  },
  variants: {},
  plugins: []
}
